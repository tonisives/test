package com.highmobility.testapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.highmobility.hmkit.Manager;
import com.highmobility.utils.Bytes;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {
    @BindView(R.id.label) TextView label;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Manager.getInstance().initialize(
                "dGVzdCICCOOn9LbO+rk1yoDGGoOzIMK2h2bTdv3RoECzpEeYrH/W9AiqxITsuZI9FoCCwqbeMFot6te/L7RS9OtkKRnXc76M6PVWixvKcM+pPB4DSkcA7Ze2oLXAkm/hZWoclprW4chwNOEh80078v3T9ZBU8dBdl0jddJ4lrUpTykl1W9sL+cI+G67npMi2AClpng7e8PMf",
                "o952qK/4lmfH+tjjowFul4v17Rar+lOlGLSbJN/50S0=",
                "HJS8Wh+Gjh2JRB8pMOmQdTMfVR7JoPLVF1U85xjSg7puYoTwLf+DO9Zs67jw+6pXmtkYxynMQm0rfcBU0XFF5A==",
                this
        );

        label.setText(Bytes.hexFromBytes(Manager.getInstance().getDeviceCertificate().getSerial()));
    }
}
